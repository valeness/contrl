#!/usr/bin/env python2.7
#
import sys
import time

sav = 'settings.ini'

# Self-signed / CA / SASL questions
def question():
    sasl_q = str(raw_input('Would you like to use '
    + 'SASL authentication? Y/N: '))
    if sasl_q.upper() == 'Y' or sasl_q.upper() == 'N':
        if sasl_q.upper() == 'N':
            sasl_var = 0
        else:
            sasl_var = 2
        cert_q = str(raw_input('Would you like to connect '
        + 'to a self-signed server? Y/N: ')) 
        if cert_q.upper() == 'Y' or cert_q.upper() == 'N':
            if cert_q.upper() == 'N':
                cert_var = 4
            else:
                cert_var = 8
            with open(sav, 'w') as sav1:
                sav1.write(str(sasl_var + cert_var))
                sav2 = sasl_var + cert_var
        else:
            print 'Input was not Y or N, stopping program.'
            sys.exit()
    else:
        print 'Input was not Y or N, stopping program.'
        sys.exit()


def log_time():
    print time.strftime('[%H:%M:%S]'),


def print_time():
    print time.strftime('[%H:%M:%S on %B %d, %Y]'),