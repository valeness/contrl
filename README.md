## Contrl IRC bot ##
- Connects to a CA / self-signed IRC server through Tor, via TLS.
- Logs all actions/messages to a file, separated by day.
- Nickserv authentication
- Ability to choose SASL authentication
- ~~Can be issued commands~~ WRONG! BEING REWORKED

### Current Release: ###
- 14.11.15 ( 2014, November, 15th )

### WONTFIX: ###
- Plain-text IRC will never be supported. TLS/SSL must be used. There is no excuse for plaintext IRC.

## Bounties!: ##
- #### #3 $7.50 ####
- #### #9 $10 ####
- #### #7 $12.50 ####
- #### #6 $3.00 for each ####
- #### #5 $20.00 ####
- #### #2 $30.00 ####

- ### If you would like to be assigned an issue with a bounty, ###
- ### let me know in irc.oftc.net #contrl - nick is UHYFU36 ###

###***Bounties must be pulled and accepted before any money changes hands.***###

## Issues: ##

Are you using the most up-to-date version of the bot?

Is your issue unique?

Have you checked the issues to ensure this hasn't been brought up by someone else?

If you've checked and made sure of all of these then please post a new issue with:
1. The bot version you're using
2. A full traceback
3. What your issue is, and how we can reproduce it
4. What operating system you're getting this issue on


### It's Implied That: ###
- **The server admin has prevented registration of the usernames "nickserv" and "chanserv"**
- You're using Pidgin, if you have a self-signed server you're connecting to

## Help! ##
If you find any bugs:
- Please check the issues to ensure it hasn't already been filed.
- If your issue is unique and hasn't been filed, please create an issue!

# Warning! Dragons Ahead!: #
This program has not been tested under any version of Windows!
This bot has only been tested on Debian and derivatives there of.

If your operating system is not Debian or a derivative, and you'd like
to test and verify if it works on your operating system, it would be
very welcome!

Please let us know if it works and if it doesn't, please file an issue
with traceback, version of OS, and which version of the bot you were using.

### Required: ###
- sudo apt-get install build-essential python-dev python-pip python-openssl
- pip install --user idna service_identity twisted txsocksx

# Editing CTCP Commands #

### FINGER: ###
- fingerReply ( Owner information )

### TIME: ###

In "CTCP TIME" there is 
```
#!python
time.asctime(time.gmtime(time.time()))
```
and this is where we set gmt time by default.

Check out [https://docs.python.org/2/library/time.html](https://docs.python.org/2/library/time.html)
for information on what to set for a specific time.

### VERSION: ###
1. versionName ( What is the name of this bot? )
2. versionNum ( Version of this bot )
3. versionEnv ( What environment? Linux, BSD, etc )


# CA Setup: #

Edit the following CHANGE_ME
lines in order to run the bot:
( ^ denotes something that can be changed to '' to show it's not in use )


1. server_password ( Server-wide password^ )
[ http://askubuntu.com/questions/181111/how-can-i-set-ircd-hybrid-server-password ]
2. nickserv_pass ( NickServ password ( ^ but **DANGEROUS** ))
3. channel_ ( Channel to join ( must start with # - like #example ))
4. server_ ( Network to join ( irc.example.net ))
5. port_ ( Port number ( 6697 ))
6. common_name ( Hostname for the certificate
( Go to Pidgin's Buddy List -> Tools -> Certificates ->
Get Info -> "Common name" for what to put here.
You can also go to the bottom of this page and view
the "Common Name" section.))
7. cert_ ( Not needed, set to '' )
8. username_ ( Bot nickname, change it to whatever you want )
9. sourceURL ( Source code for this bot )
10. key ( Channel room password )


# Self-Signed Setup: #
Edit the following CHANGE_ME
lines in order to run the bot:
( ^ denotes something that can be changed to '' to show it's not in use )

1. server_password ( Server-wide password^ )
[ http://askubuntu.com/questions/181111/how-can-i-set-ircd-hybrid-server-password ]
2. nickserv_pass ( NickServ password ( ^ but **DANGEROUS** ))
3. channel_ ( Channel to join ( must start with # - like #example ))
4. server_ ( Network to join ( irc.example.net ))
5. port_ ( Port number ( 6697 ))
6. common_name ( Hostname for the certificate
( Go to Pidgin's Buddy List -> Tools -> Certificates ->
Get Info -> "Common name" for what to put here.
You can also go to the bottom of this page and view
the "Common Name" section.))
7. cert_ ( Where to find the certificate )
Set to something like:
/home/username/.purple/certificates/x509/tls_peers/irc.example.net
8. username_ ( Bot nickname, change it to whatever you want )
9. sourceURL ( Source code for this bot )
10. key ( Channel room password )

# Common Name: #

- FREENODE: chat.freenode.net
- OFTC: irc.oftc.net


**Have fun!**