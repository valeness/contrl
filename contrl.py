#!/usr/bin/env python2.7
#
from time import strftime
from twisted.internet.defer import Deferred
from twisted.internet.endpoints import TCP4ClientEndpoint
from twisted.internet.protocol import ClientFactory
from twisted.internet.ssl import Certificate
from twisted.internet.ssl import optionsForClientTLS
from twisted.internet.task import react
from twisted.protocols.policies import SpewingFactory
from twisted.python.filepath import FilePath
from twisted.words.protocols.irc import IRCClient
from txsocksx.client import SOCKS5ClientEndpoint
from txsocksx.tls import TLSWrapClientEndpoint
from zope.interface import implementer
import defs
import os
import sys
import time

os.chdir(os.path.dirname(os.path.abspath(sys.argv[0])))

sav = 'settings.ini'

# If the log folder doesn't exist, make it
if not path.isdir('logs'):
    os.mkdir('logs')

# Make sure channel has a # in front or else
# bot commands won't work.
server_password = 'CHANGE_ME'
nickserv_pass = 'CHANGE_ME'
channel_ = '#CHANGE_ME'
server_ = 'CHANGE_ME'
port_ = CHANGE_ME
common_name = 'CHANGE_ME'
cert_ = 'CHANGE_ME'
username_ = 'CHANGE_ME'
sourceURL = 'CHANGE_ME'
key = 'CHANGE_ME'

# Certificate authority or self-signed?
if os.path.isfile(sav) and os.path.getsize(sav) > 0:
    with open(sav, 'r') as sav1:
        sav2 = sav1.readline()
    if int(sav2) == 8 or int(sav2) == 10:
        trustRootVar = Certificate.loadPEM(FilePath(cert_).getContent())
    elif int(sav2) == 4 or int(sav2) == 6:
        trustRootVar = None
    else:
        print 'Unknown option in ' + sav + ', wiping file.'
        with open(sav, 'w') as sav1:
            sav1.truncate(0)
        print 'Please run the program again.'
        sys.exit()
else:
    defs.question()
    with open(sav, 'r') as sav1:
        sav2 = sav1.readline()
    if int(sav2) == 8 or int(sav2) == 10:
        trustRootVar = Certificate.loadPEM(FilePath(cert_).getContent())
    elif int(sav2) == 4 or int(sav2) == 6:
        trustRootVar = None

# Change to the logs directory
os.chdir('logs')

# If today's log doesn't exist, make it
#
# Appending to the file further on in the
# program also creates the file if it
# doesn't exist.
if not path.isfile(strftime('%B %d, %Y.txt')):
    with open(strftime('%B %d, %Y.txt'), 'w') as log1:
        log1.truncate(0)

class TorIRC(IRCClient):

# Nick/Ident
    nickname = username_
# "Real name"
    realname = nickname
# Used to authenticate to nickserv
    password = nickserv_pass

# Bot owner info
    fingerReply = 'CHANGE_ME'

# VERSION info
    versionName = 'Contrl'
    versionNum = '14.11.15'
    versionEnv = 'CHANGE_ME'

# CTCP FINGER
    def ctcpQuery_FINGER(self, user, channel, data):
        nick = user.split('!')[0]
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] %s sent CTCP FINGER\n' % (channel, nick))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] %s sent CTCP FINGER' % (channel, nick))
        if callable(self.fingerReply):
            reply = self.fingerReply()
        else:
            reply = str(self.fingerReply)
        self.ctcpMakeReply(nick, [('FINGER', reply)])

# CTCP SOURCE
    def ctcpQuery_SOURCE(self, user, channel, data):
        sourceURLvar = sourceURL
        nick = user.split('!')[0]
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] %s sent CTCP SOURCE\n' % (channel, nick))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] %s sent CTCP SOURCE' % (channel, nick))
        if self.sourceURL:
            self.ctcpMakeReply(nick, [('SOURCE', sourceURLvar)])

# CTCP TIME
    def ctcpQuery_TIME(self, user, channel, data):
        nick = user.split('!')[0]
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] %s sent CTCP TIME\n' % (channel, nick))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] %s sent CTCP TIME' % (channel, nick))
# GMT/UTC time is set here
        self.ctcpMakeReply(nick, [('TIME', ':%s' %
        time.asctime(time.gmtime(time.time())))])

# CTCP VERSION
    def ctcpQuery_VERSION(self, user, channel, data):
        nick = user.split('!')[0]
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] %s sent CTCP VERSION\n' % (channel, nick))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] %s sent CTCP VERSION' % (channel, nick))
        if self.versionName:
            self.ctcpMakeReply(nick, [('VERSION', '%s:%s:%s' %
            (self.versionName,
            self.versionNum or '',
            self.versionEnv or ''))])

# Read topic, or topic is changed
    def topicUpdated(self, user, channel, newTopic):
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] topic is \'%s\'\n' % (channel, newTopic))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] topic is \'%s\'' % (channel, newTopic))

# Modes are added / removed
    def modeChanged(self, user, channel, set, modes, args):
        if set == False:
            setvar = '-'
        else:
            setvar = '+'
        nick = user.split('!')[0]
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] %s changed %s\'s mode to %s%s\n' % (channel, nick, args[0], setvar, modes))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] %s changed %s\'s mode to %s%s' % (channel, nick, args[0], setvar, modes))

# Connecting..
    def connectionMade(self):
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ') + 'Connecting...\n')
        print strftime('[%H:%M:%S on %B %d, %Y]'), 'Connecting...'
# SASL
        if int(sav2) == 6 or int(sav2) == 10:
            self.sendLine('CAP REQ :sasl')
###

        self.deferred = Deferred()
        IRCClient.connectionMade(self)

# SASL
    def irc_CAP(self, prefix, params):
        if params[1] != 'ACK' or params[2].split() != ['sasl']:
            print 'SASL not available, quitting'
            self.quit('')
        sasl = ('{0}\0{0}\0{1}'.format(self.nickname,
            self.password)).encode('base64').strip()
        self.sendLine('AUTHENTICATE PLAIN')
        self.sendLine('AUTHENTICATE ' + sasl)

    def irc_903(self, prefix, params):
        self.sendLine('CAP END')

    def irc_904(self, prefix, params):
        print 'SASL auth failed', params
        self.quit('')
    irc_905 = irc_904
###

# Connection lost
    def connectionLost(self, reason):
        self.deferred.errback(reason)

# Signed onto network
    def signedOn(self):
        if int(sav2) == 4 or int(sav2) == 8:
            self.msg('NickServ','id %s' % nickserv_pass)
            with open(strftime('%B %d, %Y.txt'), 'a') as log1:
                log1.write(strftime('[%H:%M:%S] ')
                + 'Authenticating to NickServ\n')
            print( strftime('[%H:%M:%S on %B %d, %Y] ')
            + 'Authenticating to NickServ')
        else:
            with open(strftime('%B %d, %Y.txt'), 'a') as log1:
                log1.write(strftime('[%H:%M:%S] ')
                + 'Authenticated with SASL\n')
            print( strftime('[%H:%M:%S on %B %d, %Y] ')
            + 'Authenticated with SASL')
        self.sendLine("JOIN %s %s" % (channel_, key))

# Joined a channel
    def joined(self, channel):
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S]') + '[%s] joined\n' % channel)
        print strftime('[%H:%M:%S on %B %d, %Y] ') + '[%s] joined' % channel

# /names
    def names_(self, channel):
        self.sendLine('NAMES %s' % channel)

    def irc_RPL_NAMREPLY(self, *nargs):
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] [') + str(nargs[1][2])
            + '] /names: ' + nargs[1][3] + '\n')
        print( strftime('[%H:%M:%S on %B %d, %Y] [') + str(nargs[1][2])
        + '] /names: ' + nargs[1][3])

# Kicked from channel
    def kickedFrom(self, channel, kicker, message):
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] kicked by %s\n' % (channel, kicker))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] kicked by %s' % (channel, kicker))
        self.join(channel)

# Received notice
    def noticed(self, username, channel, message):
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[NOTICE] [%s] %s: ' % (channel, username)
            + '%s\n' % (message.replace('', '')))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[NOTICE] [%s] %s: ' % (channel, username)
        + '%s' % (message.replace('', '')))

# Print messages sent to channel ( or PM )
    def privmsg(self, user, channel, msg):
        global USER_verified
        user = user.split('!', 1)[0]
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] %s: %s\n' % (channel, user, msg))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] %s: %s' % (channel, user, msg))

# Kicking a user
    def kick(self, channel, user, reason=None):
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] %s kicked\n' % (channel, user))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] %s kicked' % (channel, user))

### Observing users
# User joined channel
    def userJoined(self, user, channel):
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] %s joined\n' % (channel, user))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] %s joined' % (channel, user))

# User changed nick
    def userRenamed(self, oldname, newname):
        global USER_verified
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '%s changed name to %s\n' % (oldname, newname))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '%s changed name to %s' % (oldname, newname))

# /me actions
    def action(self, user, channel, data):
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] %s did action (%s)\n' % (channel, user, data))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] %s did action (%s)' % (channel, user, data))

# Some other user was kicked
    def userKicked(self, kickee, channel, kicker, message):
        global USER_verified
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] %s kicked %s with message ' % (channel, kicker, kickee)
            + '(%s)\n' % message)
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] %s kicked %s with message ' % (channel, kicker, kickee)
        + '(%s)' % message)

# User left channel
    def userLeft(self, user, channel):
        global USER_verified
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '[%s] %s left\n' % (channel, user))
        print( strftime('[%H:%M:%S on %B %d, %Y] ')
        + '[%s] %s left' % (channel, user))

# User left network
    def userQuit(self, user, quitMessage):
        global USER_verified
        with open(strftime('%B %d, %Y.txt'), 'a') as log1:
            log1.write(strftime('[%H:%M:%S] ')
            + '%s left the network\n' % (user))
        print(
            strftime('[%H:%M:%S on %B %d, %Y] ')
            + '%s left the network' % (user))
###

class TorIRCFactory(ClientFactory):
    protocol = TorIRC

def main(reactor):
    torEndpoint = TCP4ClientEndpoint(reactor, '127.0.0.1', 9050)
    ircEndpoint = SOCKS5ClientEndpoint(server_, port_, torEndpoint)
    host = common_name
    options_ = optionsForClientTLS(hostname = host.decode('utf-8'),
        trustRoot = trustRootVar,
        clientCertificate = None)
# TLS is added and removed here :)
    tlsEndpoint = TLSWrapClientEndpoint(options_, ircEndpoint)
    d = tlsEndpoint.connect(SpewingFactory(TorIRCFactory()))
    d.addCallback(lambda proto: proto.wrappedProtocol.deferred)
    return d

react(main, [])